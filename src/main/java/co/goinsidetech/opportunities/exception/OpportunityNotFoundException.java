package co.goinsidetech.opportunities.exception;

public class OpportunityNotFoundException extends RootException {

    public OpportunityNotFoundException(String message, String id) {
        super(message, message, id);
    }

    public OpportunityNotFoundException(String message) {
        super(message);
    }

}
