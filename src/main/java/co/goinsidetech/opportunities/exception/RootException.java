package co.goinsidetech.opportunities.exception;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public abstract class RootException extends Exception {

    private String customMessage;
    private String[] params;

    RootException(String customMessage) {
        this.customMessage = customMessage;
    }

    RootException(String customMessage, String... params) {
        this.customMessage = customMessage;
        this.params = params;
    }

    @Override
    public String getMessage() {
        return this.customMessage;
    }
}
