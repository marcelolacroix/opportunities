package co.goinsidetech.opportunities.security;


import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;


@Slf4j
public class JWTAuthenticationFilter extends GenericFilterBean {
    private static final String HEADER_ACCESS_TOKEN = "Authorization";

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        Authentication authentication = getAuthentication((HttpServletRequest) request);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        filterChain.doFilter(request, response);
    }


    public Authentication getAuthentication(HttpServletRequest request) {
        try {
            String token = request.getHeader(HEADER_ACCESS_TOKEN);
            if (isNull(token)) {
                return null;
            }

            String[] pieces = token.split("\\.");
            String b64payload = pieces[1];
            String subject = new String(Base64.decodeBase64(b64payload), "UTF-8");

            if (isNull(subject)) {
                return null;
            }
            Account account = parserSubject(subject);
            if(nonNull(account)){
                return new UsernamePasswordAuthenticationToken(account, null, Collections.emptyList());
            }

            return null;
        } catch (Exception e) {
            return null;
        }
    }

    private Account parserSubject(String token) {
        Account account = null;
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            account = objectMapper.readValue(objectMapper.readValue(token, HashMap.class).get("sub").toString(), Account.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return account;
    }

}
