package co.goinsidetech.opportunities.security;

import com.fasterxml.jackson.annotation.*;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "username",
        "password",
        "accountNonExpired",
        "accountNonLocked",
        "credentialsNonExpired",
        "enabled",
        "authorities",
        "empresa",
        "veiculo",
        "grupoEmpresa"
})
public class Account implements Serializable {

    @JsonIgnore
    private final Map<String, Object> additionalProperties = new HashMap<>();
    @JsonProperty("username")
    private String username;
    @JsonProperty("accountNonExpired")
    private Boolean accountNonExpired;
    @JsonProperty("accountNonLocked")
    private Boolean accountNonLocked;
    @JsonProperty("credentialsNonExpired")
    private Boolean credentialsNonExpired;
    @JsonProperty("enabled")
    private Boolean enabled;
    @JsonProperty("empresa")
    private Long empresa;
    @JsonProperty("veiculo")
    private Long veiculo;
    private Long id;
    @JsonProperty("grupoEmpresa")
    private Long grupoEmpresa;

    @JsonProperty("username")
    public String getUsername() {
        return username;
    }

    @JsonProperty("username")
    public void setUsername(final String username) {
        this.username = username;
    }

    @JsonProperty("accountNonExpired")
    public Boolean getAccountNonExpired() {
        return accountNonExpired;
    }

    @JsonProperty("accountNonExpired")
    public void setAccountNonExpired(final Boolean accountNonExpired) {
        this.accountNonExpired = accountNonExpired;
    }

    @JsonProperty("accountNonLocked")
    public Boolean getAccountNonLocked() {
        return accountNonLocked;
    }

    @JsonProperty("accountNonLocked")
    public void setAccountNonLocked(final Boolean accountNonLocked) {
        this.accountNonLocked = accountNonLocked;
    }

    @JsonProperty("credentialsNonExpired")
    public Boolean getCredentialsNonExpired() {
        return credentialsNonExpired;
    }

    @JsonProperty("credentialsNonExpired")
    public void setCredentialsNonExpired(final Boolean credentialsNonExpired) {
        this.credentialsNonExpired = credentialsNonExpired;
    }

    @JsonProperty("enabled")
    public Boolean getEnabled() {
        return enabled;
    }

    @JsonProperty("enabled")
    public void setEnabled(final Boolean enabled) {
        this.enabled = enabled;
    }

    @JsonProperty("empresa")
    public Long getEmpresa() {
        return empresa;
    }

    @JsonProperty("empresa")
    public void setEmpresa(final Long empresa) {
        this.empresa = empresa;
    }

    @JsonProperty("veiculo")
    public Long getVeiculo() {
        return veiculo;
    }

    @JsonProperty("veiculo")
    public void setVeiculo(final Long veiculo) {
        this.veiculo = veiculo;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(final String name, final Object value) {
        this.additionalProperties.put(name, value);
    }

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    @JsonProperty("grupoEmpresa")
    public Long getGrupoEmpresa() {
        return grupoEmpresa;
    }

    @JsonProperty("grupoEmpresa")
    public void setGrupoEmpresa(Long grupoEmpresa) {
        this.grupoEmpresa = grupoEmpresa;
    }
}
