package co.goinsidetech.opportunities.dto;

import lombok.*;

import java.util.List;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PageResult <T> {
    private int page;
    private int limit;
    private long totalElements;
    private List<T> elements;

    public PageResult(List<T> elements, long totalElements, int page, int limit) {
        this.elements = elements;
        this.totalElements = totalElements;
        this.page = page;
        this.limit = limit;
    }

}
