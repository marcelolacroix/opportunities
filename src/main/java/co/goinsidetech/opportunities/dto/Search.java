package co.goinsidetech.opportunities.dto;

import co.goinsidetech.opportunities.model.entity.LocalDateRange;
import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Search {

    private String id;

    private List<Long> organizations;

    private List<Long> agencies;

    private List<Long> people;

    private List<Long> products;

    private List<String> files;

    private Boolean isPublic;

    private String origin;

    private Long originId;

    private Boolean active;

    private Boolean excluded;

    private String title;

    private List<String> tags;

    private LocalDateRange tradingPeriod;

}
