package co.goinsidetech.opportunities.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
public class Error implements Serializable {

    private static final long serialVersionUID = -592876583532303478L;

    private LocalDateTime timestamp;
    private String path;
    private Integer status;
    private String error;
    private String message;

    public Error(LocalDateTime now, int status, String error, String message, String path) {
        this.timestamp = now;
        this.status = status;
        this.error = error;
        this.message = message;
        this.path = path;
    }

}
