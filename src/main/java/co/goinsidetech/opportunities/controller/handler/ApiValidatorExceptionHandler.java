package co.goinsidetech.opportunities.controller.handler;

import co.goinsidetech.opportunities.dto.Error;
import co.goinsidetech.opportunities.exception.RootException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

import java.time.LocalDateTime;

@ControllerAdvice
@RestController
@Slf4j
public class ApiValidatorExceptionHandler {

    private final MessageSource messageSource;

    @Autowired
    public ApiValidatorExceptionHandler(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request) {
        Error errorDetails = buildErrorDetails(ex, request, HttpStatus.INTERNAL_SERVER_ERROR);
        log.error(ex.getMessage(), ex);
        return new ResponseEntity(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    public static String getMessage(Exception ex, MessageSource messageSource) {
        String message;
        try {
            RootException rootEx = (RootException) ex;
            if (ArrayUtils.isNotEmpty(rootEx.getParams())) {
                message = messageSource.getMessage(ex.getMessage(), rootEx.getParams(), LocaleContextHolder.getLocale());
            } else {
                message = messageSource.getMessage(new DefaultMessageSourceResolvable(rootEx.getMessage()), LocaleContextHolder.getLocale());
            }
        } catch (NoSuchMessageException | ClassCastException e) {
            message = ex.getMessage();
        }
        return message;
    }

    private Error buildErrorDetails(Exception ex, WebRequest request, HttpStatus httpStatus) {
        log.error(ex.getMessage(), ex);
        String message = getMessage(ex, messageSource);
        return new Error(
                LocalDateTime.now(),
                httpStatus.value(),
                httpStatus.getReasonPhrase(),
                message,
                request.getContextPath());
    }

}
