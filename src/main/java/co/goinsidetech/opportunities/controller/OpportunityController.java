package co.goinsidetech.opportunities.controller;

import co.goinsidetech.opportunities.dto.PageResult;
import co.goinsidetech.opportunities.dto.Search;
import co.goinsidetech.opportunities.model.entity.Opportunity;
import co.goinsidetech.opportunities.service.OpportunityService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/opportunities")
public class OpportunityController {

    @Autowired
    private OpportunityService opportunityService;

    @PostMapping
    public ResponseEntity<?> create(@RequestBody Opportunity opportunity) throws Exception {
        return new ResponseEntity<>(opportunityService.create(opportunity), HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<?> edit(@RequestBody Opportunity opportunity) throws Exception {
        return ResponseEntity.ok(opportunityService.edit(opportunity));
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable("id") String id) throws Exception {
        return ResponseEntity.ok(opportunityService.findById(id));
    }

    @PostMapping("/search")
    public ResponseEntity<?> search(@RequestParam(value = "page", defaultValue = "0") int page,
                                    @RequestParam(value = "limit", defaultValue = "30") int limit,
                                    @RequestParam(value = "order", defaultValue = "id") String order,
                                    @RequestParam(value = "direction", defaultValue = "ASC") String direction,
                                    @RequestBody Search search) {
        Page<Opportunity> result = opportunityService.search(search, PageRequest.of(page, limit, Sort.by(Direction.fromString(direction), order)));

        return ResponseEntity.ok(new PageResult<>(result.getContent(), result.getTotalElements(), result.getNumber(), result.getSize()));
    }

}
