package co.goinsidetech.opportunities.model.entity;

import lombok.*;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Product {

    private Long id;

    private String name;

    private Long company;

    private Long companyGroup;

}
