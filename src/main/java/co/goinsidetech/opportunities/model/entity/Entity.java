package co.goinsidetech.opportunities.model.entity;

import lombok.*;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Entity {

    private Long id;

    private String name;

    private String corporateName;

    private String cpf;

    private String cnpj;

    private Boolean foreign;

}
