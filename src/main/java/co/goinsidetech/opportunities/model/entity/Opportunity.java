package co.goinsidetech.opportunities.model.entity;

import co.goinsidetech.opportunities.model.enumeration.OriginEnum;
import com.querydsl.core.annotations.QueryEntity;
import lombok.*;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.List;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@QueryEntity
@Document
public class Opportunity {

    @Id
    private ObjectId id;

    private Long tenant;

    private Long company;

    private Long companyGroup;

    private Long userId;

    private LocalDateTime createdAt;

    private List<Entity> organizations;

    private List<Entity> agencies;

    private List<Entity> people;

    private List<Product> products;

    private List<FileEntity> files;

    private Boolean isPublic;

    private OriginEnum origin;

    private Long originId;

    private Boolean active;

    private LocalDateTime excludedAt;

    private LocalDateTime updatedAt;

    private String title;

    private List<String> tags;

    private Integer quota;

    private LocalDateRange tradingPeriod;

}
