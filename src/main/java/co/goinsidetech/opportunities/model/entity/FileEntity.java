package co.goinsidetech.opportunities.model.entity;

import lombok.*;

import java.io.Serializable;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FileEntity implements Serializable {

    private String uuid;

    private String name;

    private String url;

    private Boolean mediakit;

    private String extension;

}
