package co.goinsidetech.opportunities.model.repository;

import co.goinsidetech.opportunities.model.entity.Opportunity;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface OpportunityRepository extends MongoRepository<Opportunity, ObjectId>, QuerydslPredicateExecutor<Opportunity> {
}
