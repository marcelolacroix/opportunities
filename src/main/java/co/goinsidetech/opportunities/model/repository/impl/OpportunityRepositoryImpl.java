package co.goinsidetech.opportunities.model.repository.impl;

import co.goinsidetech.opportunities.dto.Search;
import co.goinsidetech.opportunities.model.entity.Opportunity;
import co.goinsidetech.opportunities.model.entity.QOpportunity;
import co.goinsidetech.opportunities.model.repository.OpportunityRepository;
import co.goinsidetech.opportunities.security.Account;
import com.querydsl.core.BooleanBuilder;
import org.apache.commons.lang3.BooleanUtils;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import static java.util.Objects.nonNull;

@Component
public class OpportunityRepositoryImpl {

    @Autowired
    OpportunityRepository opportunityRepository;

    public Page<Opportunity> search(Search search, Pageable paginationCriteria) {
        Account account = (Account) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        QOpportunity qOpportunity = QOpportunity.opportunity;
        BooleanBuilder builder = new BooleanBuilder();

        if (nonNull(account.getGrupoEmpresa())) {
            builder.and(qOpportunity.companyGroup.eq(account.getGrupoEmpresa()));
        } else {
            builder.and(qOpportunity.company.eq(account.getEmpresa()));
        }

        if (nonNull(search.getId())) {
            builder.and(qOpportunity.id.eq(new ObjectId(search.getId())));
        }

//        if (nonNull(search.getIsPublic()) && search.getIsPublic()) {
//            builder.and(qOpportunity.isPublic.isTrue());
//        } else {
//            builder.and(qOpportunity.userId.eq(account.getId()));
//        }

        if (BooleanUtils.isTrue(search.getActive())) {
            builder.and(qOpportunity.active.isTrue());
        }

        if (BooleanUtils.isTrue(search.getExcluded())) {
            builder.and(qOpportunity.excludedAt.isNotNull());
        }

        return opportunityRepository.findAll(builder, paginationCriteria);
    }
}
