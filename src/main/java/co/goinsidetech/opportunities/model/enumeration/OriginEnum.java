package co.goinsidetech.opportunities.model.enumeration;

public enum OriginEnum {

    CRM_PRODUCT("product");

    private String param;

    OriginEnum(String param) {
        this.param = param;
    }

    public static String get(String key) {
        for (OriginEnum n : OriginEnum.values()) {
            if (key.equals(n.name()))
                return n.param();
        }
        return null;
    }

    public String param() {
        return param;
    }

}
