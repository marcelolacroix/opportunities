package co.goinsidetech.opportunities.service;

import co.goinsidetech.opportunities.dto.Search;
import co.goinsidetech.opportunities.exception.OpportunityNotFoundException;
import co.goinsidetech.opportunities.model.entity.Opportunity;
import co.goinsidetech.opportunities.model.repository.OpportunityRepository;
import co.goinsidetech.opportunities.model.repository.impl.OpportunityRepositoryImpl;
import co.goinsidetech.opportunities.security.Account;
import lombok.extern.log4j.Log4j2;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Log4j2
@Service
public class OpportunityService {

    @Autowired
    OpportunityRepository opportunityRepository;
    @Autowired
    OpportunityRepositoryImpl opportunityRepositoryImpl;

    public Opportunity create(Opportunity opportunity) throws Exception {
        Account account = (Account) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        opportunity.setId(ObjectId.get());
        opportunity.setCreatedAt(LocalDateTime.now());
        opportunity.setUpdatedAt(opportunity.getCreatedAt());
        opportunity.setActive(true);
        opportunity.setCompany(account.getEmpresa());
        opportunity.setCompanyGroup(account.getGrupoEmpresa());
        opportunity.setUserId(account.getId());
        return opportunityRepository.save(opportunity);
    }

    public Opportunity edit(Opportunity opportunity) throws Exception {
        Opportunity oldOpportunity = opportunityRepository.findById(opportunity.getId())
                .orElseThrow(() -> new OpportunityNotFoundException("opportunity.not.found.id", opportunity.getId().toString()));
        opportunity.setCompany(oldOpportunity.getCompany());
        opportunity.setCompanyGroup(oldOpportunity.getCompanyGroup());
        opportunity.setUpdatedAt(LocalDateTime.now());

        return opportunityRepository.save(opportunity);
    }

    public Page<Opportunity> search(Search search, Pageable paginationCriteria) {
        return opportunityRepositoryImpl.search(search, paginationCriteria);
    }

    public Opportunity findById(String id) throws Exception {
        return opportunityRepository.findById(new ObjectId(id))
                .orElseThrow(() -> new OpportunityNotFoundException("opportunity.not.found.id", id));
    }
}
