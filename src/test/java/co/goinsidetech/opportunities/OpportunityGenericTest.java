package co.goinsidetech.opportunities;

import co.goinsidetech.opportunities.dto.Search;
import co.goinsidetech.opportunities.model.entity.LocalDateRange;
import co.goinsidetech.opportunities.model.entity.Opportunity;
import co.goinsidetech.opportunities.model.enumeration.OriginEnum;
import co.goinsidetech.opportunities.security.JWTAuthenticationFilter;
import co.goinsidetech.opportunities.service.OpportunityService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Nullable;
import java.time.LocalDate;
import java.time.Month;
import java.util.Arrays;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public abstract class OpportunityGenericTest {

    public final String ACCESS_TOKEN = "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ7XCJhdXRob3JpdGllc1wiOltdLFwiZW1wcmVzYVwiOjEyLFwidmVpY3Vsb1wiOm51bGwsXCJpZFwiOjI3MzgsXCJncnVwb0VtcHJlc2FcIjpudWxsLFwidXNlcm5hbWVcIjpcIm1sYWNyb2l4QHRkc29mdC5jb20uYnJcIixcImFjY291bnROb25FeHBpcmVkXCI6dHJ1ZSxcImFjY291bnROb25Mb2NrZWRcIjp0cnVlLFwiY3JlZGVudGlhbHNOb25FeHBpcmVkXCI6dHJ1ZSxcImVuYWJsZWRcIjp0cnVlfSJ9.q0l9AsroFYroSTPM6Q-X8g1M3IsZT4RtNb7ZXDrKL8vfn7oq5MGxUwTClo4wQZ6OWsuNliMEiMg6zSs_88jzwQ";
    private final Long PRODUCT_ID = 1L;

    @LocalServerPort
    public Integer port;

    @Autowired
    public OpportunityService opportunityService;

    public Opportunity createOpportunity() {
        return Opportunity
                .builder()
                .active(true)
                .agencies(null)
                .files(null)
                .isPublic(true)
                .organizations(null)
                .origin(OriginEnum.CRM_PRODUCT)
                .originId(PRODUCT_ID)
                .people(null)
                .products(null)
                .quota(5)
                .tags(Arrays.asList("tag1", "tag2"))
                .title("title")
                .tradingPeriod(new LocalDateRange(LocalDate.of(2019, Month.NOVEMBER, 1), LocalDate.of(2019 ,Month.DECEMBER, 1)))
                .userId(2738L)
                .build();
    }

    public Search createSearch(@Nullable String id) {
        return Search
                .builder()
                .active(true)
                .id(id)
                .build();
    }

    @Before
    public void setSecurityContextPrincipal() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addHeader("Authorization", ACCESS_TOKEN);

        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        Authentication authentication = new JWTAuthenticationFilter().getAuthentication(request);
        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);
    }

}
