package co.goinsidetech.opportunities.controller;

import co.goinsidetech.opportunities.OpportunityGenericTest;
import co.goinsidetech.opportunities.dto.PageResult;
import co.goinsidetech.opportunities.dto.Search;
import co.goinsidetech.opportunities.model.entity.Opportunity;
import org.junit.Test;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.util.UriComponentsBuilder;

import static org.junit.Assert.*;

public class OpportunityControllerTest extends OpportunityGenericTest {

    @Test
    public void shouldCreateOpportunity() throws Exception {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", super.ACCESS_TOKEN);

        HttpEntity<Opportunity> entity = new HttpEntity<>(super.createOpportunity(), headers);

        String url = "http://127.0.0.1:" + super.port + "/opportunities";

        TestRestTemplate testRestTemplate = new TestRestTemplate();
        Opportunity result = testRestTemplate.exchange(
                url,
                HttpMethod.POST,
                entity,
                Opportunity.class).getBody();

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(opportunityService.findById(result.getId().toString()));
    }

    @Test
    public void shouldSearchOpportunity() throws Exception {
        opportunityService.create(super.createOpportunity());

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", super.ACCESS_TOKEN);

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl("http://127.0.0.1:" + super.port + "/opportunities/search")
                .queryParam("page", 0)
                .queryParam("limit", 20)
                .queryParam("order", "id")
                .queryParam("direction", "DESC");

        HttpEntity<Search> entity = new HttpEntity<>(super.createSearch(null), headers);

        TestRestTemplate testRestTemplate = new TestRestTemplate();
        PageResult<Opportunity> result = testRestTemplate.exchange(
                builder.toUriString(),
                HttpMethod.POST,
                entity,
                new ParameterizedTypeReference<PageResult<Opportunity>>() {}).getBody();

        assertNotNull(result);
        assertFalse(result.getElements().isEmpty());
    }
}
