package co.goinsidetech.opportunities.service;

import co.goinsidetech.opportunities.OpportunityGenericTest;
import co.goinsidetech.opportunities.dto.Search;
import co.goinsidetech.opportunities.model.entity.LocalDateRange;
import co.goinsidetech.opportunities.model.entity.Opportunity;
import org.junit.Test;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.time.LocalDate;
import java.time.Month;

import static org.junit.Assert.*;

public class OpportunityServiceTest extends OpportunityGenericTest {

    @Test
    public void shouldCreateANewOpportunity() throws Exception {
        Opportunity opportunity = super.createOpportunity();

        var result = opportunityService.create(opportunity);

        assertNotNull(result);
        assertNotNull(result.getId());
    }

    @Test
    public void shouldEditAnExistingOpportunity() throws Exception {
        Opportunity opportunity = opportunityService.create(super.createOpportunity());
        final String NEW_TITLE = "New title";
        opportunity.setTitle(NEW_TITLE);

        var result = opportunityService.edit(opportunity);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertEquals(result.getId(), opportunity.getId());
        assertEquals(result.getTitle(), NEW_TITLE);
    }

    @Test
    public void shouldFindOpportunity() throws Exception {
        Opportunity opportunity = opportunityService.create(super.createOpportunity());
        Search search = super.createSearch(opportunity.getId().toString());

        var result = opportunityService.search(search, PageRequest.of(0, 20, Sort.by(Sort.Order.by("id")))).get().findFirst().orElse(null);

        assertNotNull(result);
        assertEquals(result.getId(), opportunity.getId());
    }

    @Test
    public void shouldFindOpportunityOrderedByTradingPeriodEndAsc() throws Exception {
        opportunityService.create(super.createOpportunity());
        Opportunity anotherOpportunity = super.createOpportunity();
        anotherOpportunity.setTradingPeriod(new LocalDateRange(LocalDate.of(2019, Month.NOVEMBER, 1), LocalDate.of(2020, Month.JANUARY, 1)));
        opportunityService.create(anotherOpportunity);
        Search search = super.createSearch(null);

        var result = opportunityService.search(search, PageRequest.of(0, 20, Sort.by(Sort.Direction.fromString("ASC"), "tradingPeriod.end"))).getContent();

        assertNotNull(result);
        assertEquals(result.size(), 2);
        assertTrue(result.get(0).getTradingPeriod().getEnd().compareTo(result.get(1).getTradingPeriod().getEnd()) < 0);
    }

    @Test
    public void shouldFindOpportunityById() throws Exception {
        Opportunity opportunity = opportunityService.create(super.createOpportunity());

        var result = opportunityService.findById(opportunity.getId().toString());

        assertNotNull(result);
        assertEquals(result.getId(), opportunity.getId());
    }

}
