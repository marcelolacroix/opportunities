Microsserviço de oportunidades

Para rodar o projeto é necessário rodar o lifecycle do maven 'generate-sources', para isso precisamos
rodar o comando mvn clean install ou mvn compile.

Caso utilize o Intellij, ele possui uma ferramenta para gerar essas classes automaticamente, 
basta clicar com o botão direito do mouse em qualquer lugar da árvore
de arquivos do projeto ir na aba maven e então clicar em `Generate sources and Update Folders` que ele irá gerar
os arquivos.